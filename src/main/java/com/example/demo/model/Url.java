package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Url {
	
	public Url() {
			
		}
	
	public Url(String url, int redirectType) {
		this.url = url;
		this.visitCounter = 0;
		this.redirectType = redirectType;
	}
	
	public Url(String longUrl, String shortUrl, String ownerId, int redirectType) {
		this.url = longUrl;
		this.shortUrl = shortUrl;
		this.visitCounter = 0;
		this.ownerId = ownerId;
		this.redirectType = redirectType;
	}
	
	@Id @GeneratedValue
	private int urlId;
	private String url;
	private int redirectType;
	private String shortUrl;
	private int visitCounter;
	private String ownerId;
	
	
	public int getId() {
		return urlId;
	}
	public void setId(int id) {
		this.urlId = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String longUrl) {
		this.url = longUrl;
	}
	public String getShortUrl() {
		return shortUrl;
	}
	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}
	public int getVisitCounter() {
		return visitCounter;
	}
	public void setVisitCounter(int visitCounter) {
		this.visitCounter = visitCounter;
	}	
	public int getRedirectType() {
		return redirectType;
	}
	public void setRedirectType(int redirectType) {
		this.redirectType = redirectType;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	
	@Override
	public String toString() {
		return "Url [id=" + urlId + ", url=" + url + ", redirectType=" + redirectType + ", shortUrl=" + shortUrl
				+ ", visitCounter=" + visitCounter + ", ownerId=" + ownerId + "]";
	}
	
		
}
