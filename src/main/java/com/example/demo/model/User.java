package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class User {

	public User() {
		
	}

	public User(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}
	
	public User(String userName) {
		this.userName = userName;
	}

	@Id @GeneratedValue private int userId;
	private String userName;
	private String password;
	
	

	public int getId() {
		return userId;
	}

	public void setId(int id) {
		this.userId = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Override
	public String toString() {
		return "User [userName=" + userName + "]";
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
