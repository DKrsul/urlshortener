package com.example.demo.controller;

import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.httpResponses.ResponseAddAccount;
import com.example.demo.httpResponses.ResponseRegisterUrl;
import com.example.demo.httpResponses.ResponseRegisterUrlFailed;
import com.example.demo.httpResponses.ResponseStatistic;
import com.example.demo.model.Url;
import com.example.demo.model.User;
import com.example.demo.security.BasicAuthentication;
import com.example.demo.services.ShortenerService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class RestServerController {
	
	@Autowired
	ShortenerService shortener;
	
	@Autowired
	BasicAuthentication basicAuth;
	
	@PostMapping("/account")
	public ResponseAddAccount addAccount(HttpEntity<String> httpEntity) throws JsonParseException, JsonMappingException, IOException{

		String body = httpEntity.getBody();
		User newUser = new ObjectMapper().readValue(body, User.class);
	
		return shortener.createAccount(newUser);
	}
	
	@PostMapping("/register")
	public ResponseRegisterUrl registerUrl(HttpEntity<String> httpEntity) throws JsonParseException, JsonMappingException, IOException{
		
		if(basicAuth.authenticate(httpEntity)){
			Url newUrl = new ObjectMapper().readValue(httpEntity.getBody(), Url.class);
			newUrl.setOwnerId(basicAuth.getUsernameFromAuthorizationHeader(httpEntity));
			
			return shortener.registrateUrl(newUrl);
		}
		else
			return new ResponseRegisterUrlFailed("Wrong username or password.");		
	}
	
	@GetMapping("/statistic/{accountId}")
	public List<ResponseStatistic> showStatistic(@PathVariable String accountId, HttpEntity<String> httpEntity){
		
		if(basicAuth.authenticate(httpEntity))
			return shortener.getStatistic(accountId);
		
		else 
			return null;
	}
	
	@GetMapping("/list/{accountId}")
	public List<Url> getRegisteredUrls(@PathVariable String accountId, HttpEntity<String> httpEntity){
		if(basicAuth.authenticate(httpEntity))
			return shortener.getRegisteredUrls(accountId);
		
		else 
			return null;
	}
}
	
	