package com.example.demo.controller;


import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.services.ShortenerService;

@RestController
public class RedirectController {
	
	@Autowired
	ShortenerService shortener;
	
	@GetMapping("/shortUrl/{id}")
	public void redirect(@PathVariable String id, HttpServletResponse httpServletResponse) {
		String shortUrl = "localhost:8080/shortUrl/" + id;
		shortener.redirectToLongUrl(shortUrl, httpServletResponse);
	}
	
}
