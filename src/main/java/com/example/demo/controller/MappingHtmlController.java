package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import com.example.demo.dao.UrlRepo;

@Controller
public class MappingHtmlController {
	
	@Autowired
	UrlRepo urlRepo;
	
	@GetMapping("/")
	public String home() {
		return "home.html";
	}
		
	@GetMapping("/account")
	public String account() {
		return "account.html";
	}
	
	@GetMapping("/register")
	public String register() {
		return "register.html";
	}
	
	@GetMapping("/statistic")
	public String statistic() {
		return "statistic.html";
	}
	
	@GetMapping("/list")
	public String list() {
		return "list.html";
	}
	
	@GetMapping("/help")
	public String help() {
		return "help.html";
	}
	

	
}
