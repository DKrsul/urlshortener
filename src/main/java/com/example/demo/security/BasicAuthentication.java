package com.example.demo.security;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.example.demo.dao.UserRepo;
import com.example.demo.model.User;

@Configuration
public class BasicAuthentication {
	
	@Autowired
	UserRepo userRepo;
	
	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	
	public boolean authenticate(HttpEntity<String> httpEntity) {
		
		HttpHeaders headers = httpEntity.getHeaders(); 
		String authorizationHeader = headers.getFirst("Authorization");
		
		authorizationHeader = authorizationHeader.substring(6); //removing "Basic " from header
		byte[] decodedByte = Base64.getDecoder().decode(authorizationHeader);
		String authDecoded = new String(decodedByte, StandardCharsets.UTF_8);
		
		String[] authSplited = authDecoded.split(":");
		
		String username;
		String password;
		
		if(authSplited.length == 2) {
			username = authSplited[0];
			password = authSplited[1];
		}
		else return false;
		
		
		if(userRepo == null) {
			System.out.println(username);
		}
		User databaseUser = userRepo.findByUserName(username);
		
		if(databaseUser != null){
			return passwordEncoder.matches(password, databaseUser.getPassword());
		}
		else return false;
		
	}
	
	public String getUsernameFromAuthorizationHeader(HttpEntity<String> httpEntity) {
		
		HttpHeaders headers = httpEntity.getHeaders(); 
		String authorizationHeader = headers.getFirst("Authorization");
		
		authorizationHeader = authorizationHeader.substring(6); //removing "Basic " from header
		byte[] decodedByte = Base64.getDecoder().decode(authorizationHeader);
		String authDecoded = new String(decodedByte, StandardCharsets.UTF_8);
		
		String[] authSplited = authDecoded.split(":");
		
		
		if(authSplited.length == 2)
			return authSplited[0];

		else return null;
		
	}
	
}
