package com.example.demo.dao;

import java.util.List;


import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Url;


public interface UrlRepo extends CrudRepository<Url, Long> {

	public boolean existsByUrl (String url);
	
	public boolean existsByShortUrl (String shortUrl);
	
	public Url findFirstByShortUrl (String shortUrl);
	
	public List <Url> findAllByOwnerId(String ownerId);
}
