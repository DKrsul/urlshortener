package com.example.demo.services;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.example.demo.dao.UrlRepo;
import com.example.demo.dao.UserRepo;
import com.example.demo.httpResponses.ResponseAddAccount;
import com.example.demo.httpResponses.ResponseRegisterUrl;
import com.example.demo.httpResponses.ResponseRegisterUrlFailed;
import com.example.demo.httpResponses.ResponseRegisterUrlSuccessful;
import com.example.demo.httpResponses.ResponseStatistic;
import com.example.demo.model.Url;
import com.example.demo.model.User;

@Service
public class ShortenerService {
	
	@Autowired
	UrlRepo urlRepo;
	
	@Autowired
	UserRepo userRepo;
	
	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	
	public ResponseAddAccount createAccount(User newUser) {
		String userName = newUser.getUserName();
		
		if (userRepo.existsByUserName(userName))
			return new ResponseAddAccount(false, "Account with that username already exists.");
		
		else{
			String password = RandomStringUtils.random(8, true, true);
			String encodedPassword = passwordEncoder.encode(password);
			
			newUser.setPassword(encodedPassword);
			userRepo.save(newUser);
			return new ResponseAddAccount(true, "Your account is opened.", password);
		}
	}
	
	public ResponseRegisterUrl registrateUrl(Url newUrl) {
		if(urlRepo.existsByUrl(newUrl.getUrl())) 
			return new ResponseRegisterUrlFailed("This URL already exists in database.");
		
		else{
			String shortUrl = generateShortUrl();
			newUrl.setShortUrl(shortUrl);

			urlRepo.save(newUrl);
		
			return new ResponseRegisterUrlSuccessful(shortUrl);
		}
		
	}
	
	public String generateShortUrl() {
		String shortUrlRandomString = RandomStringUtils.random(8, true, true);
		String shortUrl;
		do {
			shortUrl = "localhost:8080/shortUrl/" + shortUrlRandomString;
		} while (urlRepo.existsByShortUrl(shortUrl));
		
		return shortUrl;
	}

	public List<ResponseStatistic> getStatistic(String accountId) {
		List <Url> urls = urlRepo.findAllByOwnerId(accountId);
		List<ResponseStatistic> response = new ArrayList<ResponseStatistic>();

		for(Url url:urls) 
			response.add(new ResponseStatistic(url.getUrl(), url.getVisitCounter()));
		
		return response;
	}
	
	public List<Url> getRegisteredUrls(String accountId){
		List <Url> urls = urlRepo.findAllByOwnerId(accountId);
		return urls;		
	}
	
	public void redirectToLongUrl(String shortUrl, HttpServletResponse httpServletResponse) {
		if(urlRepo.existsByShortUrl(shortUrl)) {
			Url longUrl = urlRepo.findFirstByShortUrl(shortUrl);
			longUrl.setVisitCounter(longUrl.getVisitCounter() +1);
			urlRepo.save(longUrl);
			httpServletResponse.setHeader("Location", longUrl.getUrl());
		    httpServletResponse.setStatus(longUrl.getRedirectType());
		}
	}

}
