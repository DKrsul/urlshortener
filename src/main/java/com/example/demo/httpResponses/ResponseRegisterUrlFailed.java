package com.example.demo.httpResponses;

public class ResponseRegisterUrlFailed implements ResponseRegisterUrl{
	
	public ResponseRegisterUrlFailed(String error) {
		this.error = error;
	}

	private String error;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "ResponseRegisterUrlFailed [error=" + error + "]";
	}
	
}
