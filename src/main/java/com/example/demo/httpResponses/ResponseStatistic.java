package com.example.demo.httpResponses;

public class ResponseStatistic{
	
	public ResponseStatistic() {
	
	}
	
	public ResponseStatistic(String longUrl, int visitCounter) {
		this.longUrl = longUrl;
		this.visitCounter = visitCounter;
	}
	
	private String longUrl;
	private int visitCounter;
	
	public String getLongUrl() {
		return longUrl;
	}
	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}
	public int getVisitCounter() {
		return visitCounter;
	}
	public void setVisitCounter(int visitCounter) {
		this.visitCounter = visitCounter;
	}
	
	@Override
	public String toString() {
		return "ResponseStatistic [longUrl=" + longUrl + ", visitCounter=" + visitCounter + "]";
	}
	
	
}
