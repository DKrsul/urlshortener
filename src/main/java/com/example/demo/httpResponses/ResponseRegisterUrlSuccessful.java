package com.example.demo.httpResponses;

public class ResponseRegisterUrlSuccessful implements ResponseRegisterUrl{
		
	public ResponseRegisterUrlSuccessful(String shortUrl) {
		this.shortUrl = shortUrl;
	}
	
	private String shortUrl;
	
	
	public String getShortUrl() {
		return shortUrl;
	}

	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}

	@Override
	public String toString() {
		return "ResponseRegisterUrl [shortUrl=" + shortUrl + "]";
	}

	
	
}
