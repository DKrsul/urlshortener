package com.example.demo.httpResponses;

public class ResponseAddAccount {
	
	public ResponseAddAccount(boolean success, String description, String password) {
		this.success = success;
		this.description = description;
		this.password = password;
	}
	
	public ResponseAddAccount(boolean success, String description) {
		this.success = success;
		this.description = description;
	}
	
	
	private boolean success;
	private String description;
	private String password;
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "ResponseAddAccount [success=" + success + ", description=" + description + ", password=" + password
				+ "]";
	}
	
}
