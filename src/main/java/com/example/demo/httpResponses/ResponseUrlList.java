package com.example.demo.httpResponses;

public class ResponseUrlList {
	public ResponseUrlList() {
		
	}
	
	public ResponseUrlList(String longUrl, String shortUrl) {
		this.longUrl = longUrl;
		this.shortUrl = shortUrl;
	}
	
	private String longUrl;
	private String shortUrl;
	
	public String getLongUrl() {
		return longUrl;
	}
	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}

	public String getShortUrl() {
		return shortUrl;
	}

	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}

	@Override
	public String toString() {
		return "ResponseUrlList [longUrl=" + longUrl + ", shortUrl=" + shortUrl + "]";
	}
	
	

	
}


