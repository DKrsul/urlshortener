function encodeAuthorizationHeader(user, password) {
			  var tok = user + ':' + password;
			  var hash = btoa(tok);
			  return "Basic " + hash;
			}
		
const form = document.getElementById("statisticForm");
	
form.addEventListener("submit", (e) => {
	e.preventDefault();
			  
	var username = document.getElementById("accountId").value;
	var password = document.getElementById("accountPassword").value;
	
	if(username!="" && password!=""){
		const request = new XMLHttpRequest();
		request.open( form.method, form.action + "/" + username);
		request.setRequestHeader("Accept", "application/json");
		request.setRequestHeader("Content-Type", "application/json");
		request.setRequestHeader("Authorization", encodeAuthorizationHeader(username, password));
		
		request.onreadystatechange = function () {
			  if(request.readyState === 4 && request.status === 200) {
				  console.log(request.responseText);
				  if (request.responseText!="") {
					  var response = JSON.parse(request.responseText);
					  if(response.length > 0 && response!=null){
						  
						  var table = document.getElementById("tableResults");
					      document.getElementById("tableResults").innerHTML="";
						  var header = table.createTHead();
						  var Hrow = header.insertRow(0);     
						  var Hcell1 = Hrow.insertCell(0);
						  var Hcell2 = Hrow.insertCell(1);
						  Hcell1.innerHTML = "URL";
						  Hcell2.innerHTML = "Short URL"; 
						    
						  var row;
						  var cell1;
						  var cell2;
						  
						  for(i=0; i<response.length; i++){
							  row = table.insertRow(-1);
							  cell1 = row.insertCell(0);
							  cell2 = row.insertCell(1);
							  cell1.innerHTML = "<a href='" + response[i].url + "'>" + response[i].url + "</a>";
							  cell2.innerHTML = "<a href='" + response[i].shortUrl.substring(15) + "'>" + response[i].shortUrl + "</a>";
						  }
						  
						  document.getElementById("results").style.display="block";
					  }
					  
					  else{
						  document.getElementById("errorMessage").style.display="block";
						  document.getElementById("errorText").innerHTML = "You haven't registered any URL yet.";
					  }
			 	 }
				 else{
					 document.getElementById("errorMessage").style.display="block";
					 document.getElementById("errorText").innerHTML = "Wrong username or password.";
				 }
			}
		}
		
			request.send();
	}	
	else{
		  document.getElementById("errorMessage").style.display="block";
		  document.getElementById("errorText").innerHTML = "You didn't enter username or password.";
	}
});
