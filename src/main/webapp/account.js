function encodeAuthorizationHeader(user, password) {
		  var tok = user + ':' + password;
		  var hash = btoa(tok);
		  return "Basic " + hash;
		}


const form = document.getElementById("acountRegistration");
		
		form.addEventListener("submit", (e) => {
			e.preventDefault();
			
			var accountId = document.getElementById("accountId").value;
			
			if(accountId=="") {
				document.getElementById("accountError").style.display="block";
				document.getElementById("accountError").innerHTML = "You didn't enter your username";
			}
			else{
				
				var idJson = { "userName" : accountId};
				
				const request = new XMLHttpRequest();
				request.open( form.method, form.action);
				
				request.setRequestHeader("Accept", "application/json");
				request.setRequestHeader("Content-Type", "application/json");
				
				request.onreadystatechange = function () {
				  if(request.readyState === 4 && request.status === 200) {
					var response = JSON.parse(request.responseText);
					console.log(response);
					if(response.success == true){
						document.getElementById("accountForm").style.display="none";
						document.getElementById("accountSuccess").style.display="block";
						document.getElementById("accountError").style.display="none";
						document.getElementById("responseError").style.display="none";
						document.getElementById("password").innerHTML = "Your password is: " + response.password;
					}
					else{
						document.getElementById("accountError").style.display="block";
						document.getElementById("accountError").innerHTML = response.description;
					}
				  }
				
				}	
	
				request.send(JSON.stringify(idJson));
			}
			
		});