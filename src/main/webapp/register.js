function encodeAuthorizationHeader(user, password) {
			  var tok = user + ':' + password;
			  var hash = btoa(tok);
			  return "Basic " + hash;
}

const form = document.getElementById("urlRegistration");

form.addEventListener("submit", (e) => {		
	e.preventDefault();
	
	document.getElementById("shortUrlDisplay").style.display="none";
	document.getElementById("errorDisplay").style.display="none";
	
	var username = document.getElementById("accountId").value;
	var password = document.getElementById("accountPassword").value;
	var longUrl = document.getElementById("longUrl").value;
	var redirectType = document.getElementById("redirectType").value;
	
	
	if(username!="" && password!="" && longUrl!=""){
		var bodyJson = { "url" : longUrl, "redirectType" : redirectType};
		const request = new XMLHttpRequest();
		request.open( form.method, form.action);
		request.setRequestHeader("Accept", "application/json");
		request.setRequestHeader("Content-Type", "application/json");
		request.setRequestHeader("Authorization", encodeAuthorizationHeader(username, password));
		
		request.onreadystatechange = function () {
			  if(request.readyState === 4 && request.status === 200) {
				  var response = JSON.parse(request.responseText);
				  if(response.hasOwnProperty("shortUrl")){
					  document.getElementById("shortUrlDisplay").style.display="block";
					  document.getElementById("shortUrlDisplay").innerHTML = "Registration successful. Short url: " + response.shortUrl;
				  }
				  else if (response.hasOwnProperty("error")){
					  document.getElementById("errorDisplay").style.display="block";
					  document.getElementById("errorDisplay").innerHTML = response.error;
				  }
			  }
		}		
		
		request.send(JSON.stringify(bodyJson));
	}
	else {
		document.getElementById("errorDisplay").style.display="block";
	 	document.getElementById("errorDisplay").innerHTML = "You didn't enter all required data.";
	}
	
});
